export default {
  publicPath: process.env.NODE_ENV === 'production'
      ? '/bmi-calculator/'
      : '/',

  // Target (https://go.nuxtjs.dev/config-target)
  target: 'static',

  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    title: 'bmi-calculator',
    meta: [
      { charset: 'utf-8' },
      // { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { name: 'viewport', content: 'width=350px' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/bmi-calculator/favicon.ico' },
      { rel: 'stylesheet', href: 'https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css' }
    ]
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: [
  ],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [
  ],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    // https://go.nuxtjs.dev/bootstrap
    // 'bootstrap-vue/nuxt'
  ],
  // bootstrapVue: {
  //   componentPlugins: [
  //     'FormPlugin',
  //     'FormInputPlugin'
  //   ]
  // },

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {
  },
  /**
  * Gitlab
  */
  router: {
    base: '/bmi-calculator/',  
  },
  generate: {
    dir: 'public',
  },
  // https://stackoverflow.com/a/56350630/1666623
  ssr: false

}
